### Design & Solution Choises on RAML API using Mule
1. Design the RAML-REST API using Anypoint Platform.
2. Create Mule project and import the “RAML” file created from Step 1 using Anypoint Studio.
3. Generate Mule Flows from RAML file and run and test the interface API 
4. Implement the API using different elements (HTTP, JSON To Object, Database, Transform Message, Logger) in Mule Palette
5. Intergrate interface & implementation phases using Flow Reference.
6. Add exception to the API using Reference Exception Strategy,Choice_Exception_Strategy & Catch Exception Strategy elements.
7. Create unit tests on 4 verbs CRUD. 

###Instruction
1. Install mysql and run the mysql_script.sql located in database folder.
2. Change the mysql connection in HTTP element.
3. Run raml-rest project and test the API by using either APIkits console or on Postman by accessing http:://localhost/api:8081